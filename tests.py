from datetime import datetime, timedelta
import unittest
from app import create_app, db
from app.models import User, Post
from config import Config

class TestConfig(Config):
	Testing = True
	SQLALCHEMY_DATABASE_URI = 'sqlite://'

class UserModelCase(unittest.TestCase):
	def setUp(self):
		self.app = create_app(TestConfig)
		self.app_context = self.app.app_context()
		self.app_context.push()
		db.create_all()

	def tearDown(self):
		db.session.remove()
		db.drop_all()
		self.app_context.pop()

	def test_password_hashing(self):
		admin = User(username = 'admin')
		admin.set_password('admin123')
		self.assertFalse(admin.check_password('dogin13'))
		self.assertTrue(admin.check_password('admin123'))

	def test_avatar(self):
		user = User(username = 'john', email = 'jonh@example.com')
		self.assertEqual(user.avatar(128), ('https://www.gravatar.com/avatar/5a812105645663f71fde6104cd35b10b?d=identicon&s=128'))

	def test_follow(self):
		user = User(username = 'user', email = 'user@test.com')
		admin = User(username = 'admin', email = 'admin@test.com')
		db.session.add_all([user, admin])
		db.session.commit()
		self.assertEqual(user.followed.all(), [])
		self.assertEqual(admin.followers.all(), [])

		user.follow(admin)
		db.session.commit()
		self.assertTrue(user.is_following(admin))
		self.assertEqual(user.followed.count(), 1)
		self.assertEqual(user.followed.first().username, 'admin')
		self.assertEqual(admin.followers.count(), 1)
		self.assertEqual(admin.followers.first().username, 'user')

		user.unfollow(admin)
		db.session.commit()
		self.assertFalse(user.is_following(admin))
		self.assertEqual(user.followed.count(), 0)
		self.assertEqual(admin.followers.count(), 0)

	def test_follow_posts(self):
		user1 = User(username = 'user1', email = 'user1@test.com')
		user2 = User(username = 'user2', email = 'user2@test.com')
		user3 = User(username = 'user3', email = 'user3@test.com')
		user4 = User(username = 'user4', email = 'user4@test.com')
		db.session.add_all([user1, user2, user3, user4])

		now = datetime.utcnow()

		post1 = Post(body = "from user1", author = user1, timestamp = now + timedelta(seconds = 1))
		post2 = Post(body = "from user2", author = user2, timestamp = now + timedelta(seconds = 4))
		post3 = Post(body = "from user3", author = user3, timestamp = now + timedelta(seconds = 3))
		post4 = Post(body = "from user4", author = user4, timestamp = now + timedelta(seconds = 2))
		db.session.add_all([post1, post2, post3, post4])
		db.session.commit()

		user1.follow(user2)
		user1.follow(user4)
		user2.follow(user3)
		user3.follow(user4)
		db.session.commit()

		result1 = user1.followed_posts().all()
		result2 = user2.followed_posts().all()
		result3 = user3.followed_posts().all()
		result4 = user4.followed_posts().all()
		self.assertEqual(result1, [post2, post4, post1])
		self.assertEqual(result2, [post2, post3])
		self.assertEqual(result3, [post3, post4])
		self.assertEqual(result4, [post4])

if __name__ == '__main__':
	unittest.main(verbosity = 2)