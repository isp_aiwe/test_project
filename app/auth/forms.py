# -*- coding: utf-8 -*-
from flask_wtf import FlaskForm
from flask_babel import _, lazy_gettext as _l
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo
from app.models import User

class LoginForm(FlaskForm):
	username = StringField(_l('Имя пользователя'), validators=[DataRequired()])
	password = PasswordField(_l('Пароль'), validators=[DataRequired()])
	remember_me = BooleanField(_l('Запомнить'))
	submit = SubmitField(_l('Войти'))

class RegistrationForm(FlaskForm):
	username = StringField(_l('Имя пользователя'), validators=[DataRequired()])
	email = StringField(_l('Почта'), validators=[DataRequired(), Email()])
	password = PasswordField(_l('Пароль'), validators=[DataRequired()])
	password2 = PasswordField(_l('Повторите пароль'), validators=[DataRequired(), EqualTo('password')])
	submit = SubmitField(_l('Зарегистрироваться'))

	def validate_username(self, username):
		user = User.query.filter_by(username=username.data).first()
		if user is not None:
			raise ValidationError(_('Пожалуйста используйте другое имя пользователя.'))

	def validate_email(self, email):
		user = User.query.filter_by(email=email.data).first()
		if user is not None:
			raise ValidationError(_('Пожалуйста используйте другой адресс почты.'))

class ResetPasswordRequestForm(FlaskForm):
	email = StringField(_l('Почта'), validators=[DataRequired(), Email()])
	submit = SubmitField(_l('Запрос на сброс пароля.'))

class ResetPasswordForm(FlaskForm):
	password = PasswordField(_l('Пароль'), validators = [DataRequired()])
	password2 = PasswordField(_l('Повторите пароль'), validators = [DataRequired(), EqualTo('password')])
	submit = SubmitField(_l('Изменить пароль'))