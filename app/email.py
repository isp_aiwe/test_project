# -*- coding: utf-8 -*-
import smtplib
from threading import Thread
from flask import current_app
from flask_mail import Message
from app import mail

def send_email(subject, sender, to, recipients, text_body, html_body):
	message = Message(subject, sender = sender, recipients = recipients)
	message.body = text_body
	message.html = html_body
	Thread(target = send_async_email, args = (current_app._get_current_object(), to, message)).start()

def send_async_email(app, to, message):
	with app.app_context():
		mailserver = smtplib.SMTP_SSL('smtp.yandex.ru:465')
		mailserver.login(app.config['MAIL_USERNAME'], app.config['MAIL_PASSWORD'])
		mailserver.sendmail(app.config['MAIL_USERNAME'], to, str(message))
		mailserver.quit()
