import json
import requests
from flask import current_app
from flask_babel import _

def translate(text, source_language, dest_language):
	if 'TRANSLATOR_API_KEY' not in current_app.config or not current_app.config['TRANSLATOR_API_KEY']:
		return _('Ошибка: сервис перевода не настроен')
	r = requests.get('https://translate.yandex.net/api/v1.5/tr.json/translate?key={}&text={}&lang={}-{}'.format(current_app.config['TRANSLATOR_API_KEY'], text, source_language, dest_language))
	if r.status_code !=200:
		return _('Ошибка: сервис перевода не работает')
	return json.loads(r.content.decode('utf-8-sig'))['text'][0]