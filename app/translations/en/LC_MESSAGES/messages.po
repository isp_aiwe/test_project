# English translations for PROJECT.
# Copyright (C) 2020 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2020-02-27 18:02+0300\n"
"PO-Revision-Date: 2020-02-25 11:15+0300\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language: en\n"
"Language-Team: en <LL@li.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: app/__init__.py:21
msgid "Пожалуйста, войдите, чтобы открыть эту страницу."
msgstr "Please, log in to open this page "

#: app/translate.py:8
msgid "Ошибка: сервис перевода не настроен"
msgstr "Error: translate service is not configured"

#: app/translate.py:11
msgid "Ошибка: сервис перевода не работает"
msgstr "Error: transalte service does not work"

#: app/auth/email.py:7 app/templates/auth/reset_password.html:5
#: app/templates/auth/reset_password_request.html:5
msgid "Сброс пароля"
msgstr "Reset password"

#: app/auth/forms.py:9 app/auth/forms.py:15 app/main/forms.py:9
msgid "Имя пользователя"
msgstr "Username"

#: app/auth/forms.py:10 app/auth/forms.py:17 app/auth/forms.py:36
msgid "Пароль"
msgstr "Password"

#: app/auth/forms.py:11
msgid "Запомнить"
msgstr "Remember"

#: app/auth/forms.py:12
msgid "Войти"
msgstr "Sign in"

#: app/auth/forms.py:16 app/auth/forms.py:32
msgid "Почта"
msgstr "Email"

#: app/auth/forms.py:18 app/auth/forms.py:37
msgid "Повторите пароль"
msgstr "Repeat password"

#: app/auth/forms.py:19
msgid "Зарегистрироваться"
msgstr "Register"

#: app/auth/forms.py:24 app/main/forms.py:21
msgid "Пожалуйста используйте другое имя пользователя."
msgstr "Please use a different username"

#: app/auth/forms.py:29
msgid "Пожалуйста используйте другой адресс почты."
msgstr "Please use a different email address"

#: app/auth/forms.py:33
msgid "Запрос на сброс пароля."
msgstr "Request to reset password"

#: app/auth/forms.py:38
msgid "Изменить пароль"
msgstr "Change password"

#: app/auth/routes.py:19
msgid "Некорректный пароль или имя пользователя"
msgstr "Invalid password or username"

#: app/auth/routes.py:26 app/templates/auth/login.html:5
msgid "Вход"
msgstr "Sign in"

#: app/auth/routes.py:43
msgid "Поздравляю, теперь вы зарегистрированный пользователь!"
msgstr "Congratulations, now you are new register user!"

#: app/auth/routes.py:45 app/templates/register.html:5
msgid "Регистрация"
msgstr "Register"

#: app/auth/routes.py:56
msgid "Проверьте вашу почту, мы отправили вам письмо с инструкцией сброса пароля"
msgstr "Check your email, we send your a letter with instructions for reset password"

#: app/auth/routes.py:71
msgid "Ваш пароль был успешно изменен."
msgstr "Your password has been reset"

#: app/main/forms.py:10
msgid "Обо мне"
msgstr "About me"

#: app/main/forms.py:11
msgid "Сохранить"
msgstr "Save"

#: app/main/forms.py:24 app/main/forms.py:38
msgid "Напишите что-нибудь"
msgstr "Write something"

#: app/main/forms.py:25
msgid "Опубликовать"
msgstr "Publish"

#: app/main/forms.py:28 app/main/routes.py:118
msgid "Поиск"
msgstr "Find"

#: app/main/forms.py:39
msgid "Отправить"
msgstr "Send"

#: app/main/routes.py:32
msgid "Теперь ваш пост виден для всех"
msgstr "Now you post visible for all"

#: app/main/routes.py:38
msgid "Главная"
msgstr "Home page"

#: app/main/routes.py:48 app/templates/base.html:49
msgid "Профиль"
msgstr "Profile"

#: app/main/routes.py:58
msgid "Ваши изменения были сохранены"
msgstr "Your changes has been saved"

#: app/main/routes.py:63 app/templates/edit_profile.html:5
msgid "Изменить Профиль"
msgstr "Edit Profile"

#: app/main/routes.py:70 app/main/routes.py:85
#, python-format
msgid "Пользователь %(username)s не найден"
msgstr "User %(username)s not found"

#: app/main/routes.py:73
msgid "Вы не можете подписаться на самого себя!"
msgstr "You cannot follow yourself!"

#: app/main/routes.py:77
#, python-format
msgid "Вы подписались на %(username)s"
msgstr "You following %(username)s"

#: app/main/routes.py:88
msgid "Вы не можете отписаться от самого себя!"
msgstr "You cannot unfollow yourself!"

#: app/main/routes.py:92
#, python-format
msgid "Вы отписались от %(username)s"
msgstr "You unfollowing %(username)s"

#: app/main/routes.py:102 app/templates/base.html:22
msgid "Проводник"
msgstr "Explore"

#: app/main/routes.py:136
msgid "Ваше сообщение было отправлено"
msgstr "Your message has been sending"

#: app/main/routes.py:138
msgid "Отправить Сообщение"
msgstr "Send message"

#: app/templates/_post.html:16
#, python-format
msgid "%(username)s сказал %(when)s"
msgstr "%(username)s said %(when)s"

#: app/templates/_post.html:22
msgid "Перевести"
msgstr "Translate"

#: app/templates/base.html:4 app/templates/base.html:17
msgid "Блог"
msgstr "Blog"

#: app/templates/base.html:4
msgid "Добро пожаловать в Блог"
msgstr "Welcome to Blog"

#: app/templates/base.html:21
msgid "Домой"
msgstr "Home"

#: app/templates/base.html:40
msgid "Логин"
msgstr "Log in"

#: app/templates/base.html:44 app/templates/messages.html:4
msgid "Сообщения"
msgstr "Messages"

#: app/templates/base.html:50
msgid "Выход"
msgstr "Log out"

#: app/templates/base.html:87
msgid "Ошибка: нет соединения с сервером"
msgstr "Error: no connection to the server"

#: app/templates/explore.html:4 app/templates/index.html:5
#, python-format
msgid "Привет, %(username)s!"
msgstr "Hi, %(username)s"

#: app/templates/explore.html:12 app/templates/index.html:17
#: app/templates/user.html:32
msgid "Свежие посты"
msgstr "Newer posts"

#: app/templates/explore.html:17 app/templates/index.html:22
#: app/templates/user.html:37
msgid "Старые посты"
msgstr "Older posts"

#: app/templates/messages.html:11
msgid "Новые Сообщения"
msgstr "Newest messages"

#: app/templates/messages.html:14
msgid "Старые Сообщения"
msgstr "Older messages"

#: app/templates/search.html:4
msgid "Результаты поиска"
msgstr "Result of search"

#: app/templates/search.html:13
msgid "Предыдущие результаты"
msgstr "Previous results"

#: app/templates/search.html:18
msgid "Следующие результаты"
msgstr "Following results"

#: app/templates/send_message.html:5
#, python-format
msgid "Отправить сообщение для %(recipient)s"
msgstr "Send a message for %(recipient)s"

#: app/templates/user.html:8
#, python-format
msgid "Пользователь: %(username)s"
msgstr "User: %(username)s"

#: app/templates/user.html:10 app/templates/user_popup.html:8
msgid "Был в системе"
msgstr "Last seen on"

#: app/templates/user.html:11 app/templates/user_popup.html:9
#, python-format
msgid "%(count)d подписчики"
msgstr "%(count)d followers"

#: app/templates/user.html:11 app/templates/user_popup.html:9
#, python-format
msgid "%(count)d подписан"
msgstr "%(count)d followed"

#: app/templates/user.html:13
msgid "Изменить свой профиль"
msgstr "Edit your profile"

#: app/templates/user.html:15 app/templates/user_popup.html:12
msgid "Подписаться"
msgstr "Follow"

#: app/templates/user.html:17 app/templates/user_popup.html:14
msgid "Отписаться"
msgstr "Unfollow"

#: app/templates/user.html:20
msgid "Отправить приватное сообщение"
msgstr "Send private message"

#: app/templates/auth/login.html:12
msgid "Новый пользователь?"
msgstr "New user?"

#: app/templates/auth/login.html:12
msgid "Нажми для регистрации!"
msgstr "Click it to register!"

#: app/templates/auth/login.html:13
msgid "Забыли пароль?"
msgstr "Forgot your password?"

#: app/templates/auth/login.html:13
msgid "Нажми чтобы сбросить его"
msgstr "Click to reset it"

#: app/templates/errors/404.html:4 app/templates/errors/500.html:4
msgid "Страница не найдена"
msgstr "Page not found"

#: app/templates/errors/404.html:5 app/templates/errors/500.html:6
msgid "Назад"
msgstr "Back"

#: app/templates/errors/500.html:5
msgid "Администратор был уведомлен о проблеме. Извините за неудобства!"
msgstr "Administrator has been notified. Sorry for the inconvenience!"

#~ msgid "%(username)s сказал %(when)s"
#~ msgstr "%(username)s said %(when)s"

#~ msgid "Блог"
#~ msgstr "Blog"

#~ msgid "Добро пожаловать в Блог"
#~ msgstr "Welcome to Blog"

#~ msgid "Домой"
#~ msgstr "Home"

#~ msgid "Проводник"
#~ msgstr "Explore"

#~ msgid "Логин"
#~ msgstr "Login"

#~ msgid "Профиль"
#~ msgstr "Profile"

#~ msgid "Выход"
#~ msgstr "Log out"

